// console.log("Hello!");

// [Section] Objects
	/*
		- An object is a data type that is used to represent real world objects.
		- It is a collection of related data and/or functionalities/method.
		- Information stored in objects are represented in a "key:value" pair
		- Key is also mostly referred to as "property" of an object.
		- Different data type may be stored in an object's property creating data structures.
	*/
// Creating objects using object initializers/ literal notation

/*
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
	- This creates/declares an object and also initializes/assign it's properties upon creation.
	- A cellphone is an example of real world object
	- It has its own properties such as name, color, weight, unit model and a lot of other properties.

*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating using literal notation: ");
console.log(cellphone);

// Creating objects using constructor function

/*
	- Creats a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concreate occurence of any object which emphasize distinc/unique identity of it
	- Syntax:
		function objectName(valueA, valueB){
			this.keyA = value A,
			this.keyB = valueB
		}
*/
	// This is an constructor function
	// this keyword allows us to assign a new object's properties by associating them with values received from a contructor function's parameter.

	function Laptop(name, manufactureDate){
		this.laptopName = name;
		this.laptopManufactureDate = manufactureDate;
	}

	// Instatiation
		// The "new" operator creates an instance of an object
		// Objects and instances are often interchange because object literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/unique objects

	let laptop = new Laptop("Lenovo", 2008);
	console.log("Result from creating objects using object Constructor: ");
	console.log(laptop);

	let myLaptop = new Laptop("MacBook Air", 2020);
	console.log("Result from creating objects using object Constructor: ");
	console.log(myLaptop);

	let oldLaptop = Laptop("Portal R2E CCMC", 1980, "500 mb");
		/*
			- The example above invoke/calls the laptop function instead of creating a new object
			- It will return "undefined" without the "new" operator because the laptop function does not have any return statement

		*/

	console.log("Result from creating objects withot the new keywor: ");
	console.log(oldLaptop);

	/*Mini-activity*/
	// You will create a constructor function that will let us instatiate a new object


	function Menu(name, price){
		this.name = name;
		this.price = price;
	}

	let menu1 = new Menu("Adobo", 60);
	console.log("Result from creating objects using object Constructor: ");
	console.log(menu1);

	// creating empty objects
	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);

	// Accessing objects

	let array = [laptop, myLaptop]
	console.log(array);
	console.log(array[0]);
	console.log(array[0]["laptopName"]);
	// dot notation
	console.log(array[0].laptopName);

	// [Section] Initializing/adding/deleting/reassigning Object properties.
	/*
		-Like any other variable in JavaScript, objects have their properties initialized/ added after the object was created/delcared.
	*/

	let car = {};

	// Initializing/adding object properties using dot notation

	car.name = "Honda Civic";

	console.log(car);

	// Initializing/adding object property using bracket notation

	car["manufactureDate"] = 2019;

	console.log(car);

	// deleting object properties

		// deleting using bracket notation
	// delete car["name"];
	// console.log(car);
		// deleting using dot notation
	delete car.manufactureDate;
	console.log(car);

	// reassigning object properties
		// reassign object using dot notation
		car.name = "Dodge Charger R/T";
		console.log(car);
		// reassign object property using bracket notation
		car["name"] = "Jeepney";
		console.log(car);

	// [Section] Object Methods
		// A method is a function which is a property of an object
		// They are also functions and one of the key differences they have is that methods are functions related to a specific object

	let person = {
		name: "John",
		talk: function(){
			console.log("Hello my name is " + this.name);
		}
	}
	person.talk();

	// add method to objects
	person.walk = function(){
		console.log(this.name + " walked 25 steps forward.")
	}

	person.walk();

	// methods are useful for creating reusable functions that perform tasks related to objects.
	let friends = {
		firstName: "Joe",
		lastName: "Smith",
		address: {
			city: "Austin",
			country: "Texas"
		},
		emails:["joe@mail.com", "joesmith@email.xyz"],
		introduce: function(){
			console.log("Hello my name is " +this.firstName + " " + this.lastName + ". I live in " + this.address.city + " " + this.address.country + ". My emails are " + this.emails[0] + " and " + this.emails[1]);
		}
	}

	friends.introduce();

